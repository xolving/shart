package com.xolving.shart

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShartApplication

fun main(args: Array<String>) {
    runApplication<ShartApplication>(*args)
}
